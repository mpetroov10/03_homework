def is_anagram(fst_word, snd_word):
    fst_word = fst_word.lower()
    snd_word = snd_word.lower()
    if len(fst_word) != len(snd_word):
        return False
    for letter in range(len(fst_word)):
        if fst_word[letter] not in snd_word:
            return False
            
    return True
print(is_anagram("BRADE", "BeaRD"))
print(is_anagram("TOP_CODER", "COTO_PRODE"))
